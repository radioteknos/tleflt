/*
 * TRK - Satellite tracking program based on Norad SGP/SDP model with
 *       curses interface
 *
 *	by Lapo Pieri IK5NAX  2000-2011
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to ik5nax@radioteknos.it
 */

#define _GNU_SOURCE /* strcasstr() is GNU specific and this definition 
		       must be placed before _any_ #include */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "tleflt.h"


int main(int argc, char **argv){
  int clo, rv, lc, i;
  FILE *flt, *in;
  char elem[3][LINELEN], *fltit, linebuff[LINELEN], *lb;
  int *isfound;
  int option_index=0;
  static struct option long_options[]=
    {
     {"version", no_argument, 0, 0},
     {0, 0, 0, 0}
    };

  
  if(argc>1){
    while(1){
      clo=getopt_long(argc, argv, "h", long_options, &option_index);
      if(clo==-1) break;
      switch(clo){
      case 'h': 
	usage();
	return 0;
      case 0:
	if(strcmp(long_options[option_index].name, "version")==0){
	  printf("%s\n", __VERSION);
	  return 0;
	}
	break;
      case '?':
	/* printf("unknown option: %s\n", long_options[option_index].name); */
	return 1;
	break;
      }
    }

    if(argc<3){
      usage();
      return 1;
    }
    
    /* first of all store flt */
    if((flt=fopen(argv[optind], "r"))==NULL){
      printf("Unable to open filter file %s\n", argv[optind]);
      return 1;
    }

    lc=0;
    while(fgets(linebuff, LINELEN-1, flt)!=NULL){
      lc++;
    }

    rewind(flt);

    fltit=(char *)malloc(lc*LINELEN*sizeof(char));
    isfound=(int *)malloc(lc*LINELEN*sizeof(int));
    for(i=0; i<lc; i++){
      isfound[i]=0;
    }
    
    for(i=0; i<lc; i++){
      fgets(linebuff, LINELEN-1, flt);
      lb=linebuff;
      while(lb[0]==' ' || lb[0]=='\t') lb++; /* strip leading spcs & tabs */
      if(lb[0]=='#')                         /* ignore comment lines */
	continue;
      if(strlen(lb)==0)                      /* ignore blank lines */
	continue;
      /* strcpy(fltit[LINELEN*i], lb); */
      while(lb[strlen(lb)-1]=='\n' || lb[strlen(lb)-1]=='\r')
	lb[strlen(lb)-1]='\0'; 
      strcpy(fltit+(LINELEN*i), lb);
    }
        
    while(argc-optind>1){
      optind++;      
      if((in=fopen(argv[optind], "r"))==NULL){
	printf("Unable to open source %s\n", argv[optind]);
	return 1;
      }
      while((rv=getelem(in, elem))!=-1){
	if(rv==0){
	  for(i=0; i<lc; i++)
	    if(strcasestr(fltit+i*LINELEN, elem[0])!=NULL)
	      break;
	  if(i<lc){
	    printf("%s\n%s\n%s\n", elem[0], elem[1], elem[2]);
	    isfound[i]=1;
	  }
	}
	else
	  printf("internal error: getelm returns %d\n", rv);
      }
      fclose(in);
    }

    for(i=0; i<lc; i++){
      if(isfound[i]==0 && strlen(fltit+i*LINELEN)!=0){
	fprintf(stderr, "\n%s not found!\n", fltit+i*LINELEN);
      }
    }


  }

  else{
    usage();
    return 1;
  }

  return 0;
}

int getelem(FILE *fd, char elem[][LINELEN]){
  char linebuff[LINELEN], *lb;
  int chkl=-1;

  while(1){
    if(fgets(linebuff, LINELEN-1, fd)==NULL)
      return -1;
    lb=linebuff;
    while(lb[0]==' ' || lb[0]=='\t') lb++;   /* strip leading spcs & tabs */
    if(lb[0]=='#')                           /* ignore comment lines */
      continue;
    if(strlen(lb)==0)                        /* ignore blank lines */
      continue;
    if(lb[0]=='1'){
      if(chkl!=0)
	return 1;                            /* line '1' readed but in the
						wrong place */    
    }
    else if(lb[0]=='2'){
      if(chkl!=1)
	return 2;                            /* line '2' readed but in the
						wrong place */
    }
    else{
      if(chkl!=-1)
	return 3;                            /* line '0' readed but in the
						wrong place */
    }
    chkl++;
    strcpy(elem[chkl], lb);
    if((lb=strchr(elem[chkl], '\n'))!=NULL)
      *lb='\0';
    if((lb=strchr(elem[chkl], '\r'))!=NULL)
      *lb='\0';

    if(chkl==2)
      return 0;
  }

}


void usage(void){
  printf(ANSI_FG_GREEN ANSI_BOLD "\ntleflt %s" ANSI_RESET , __VERSION);
  printf(" - a filter for TLE file\n");
  printf("written by Lapo Pieri IK5NAX  https://gitlab.com/radioteknos/\n\n");
  printf("tleflt usage:\n\n");
  printf("  tleflt [-h] [--version] <flt> <infile1> [infile2]...\n\n");
  printf("  -h : this help\n");
  printf("  --version : print versione and exit\n\n");
  printf("  <infileN> are standard TLE files\n");
  printf("  <flt> file is a list of satellite names\n");
  printf("        alias are allowed separated by OR (|) sign\n");
  printf("\nnot founded sats are printed on stderr; several <infile> can be used\n\n");
}
