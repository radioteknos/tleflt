## tleflt - a filter for TLE file
A simple filter for standard Two Line Element file (satellites orbital
data)

# compile
Just do 

    $ make

Installation is not yet available; copy tleflt 

    $ sudo cp tleflt /usr/bin

# how to use
Run tleflt with no args or with -h

    $ tleflt -h
	
to get instructions
