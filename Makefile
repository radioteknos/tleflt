#package=tleflt
package=tleflt
#version=1.1
version=1.1
#tarname=$(package)
tarname=tleflt
distdir=$(tarname)-$(version)

#prefix=/usr/local
prefix=/usr/local

CFLAGS= -W -Wall -Wextra -Wshadow  -fomit-frame-pointer -g
LIBS=


$(package):  tleflt.o
	gcc $(CFLAGS) $(LDFLAGS) -o $(package) *.o

$(BUILD_DIR)/%.o: %.c
	gcc $(CFLAGS) -c -o $@ $<


install:
	install -d $(prefix)/bin
	install -m 0755 $(package) $(prefix)/bin

uninstall:
	-rm $(prefix)/bin/$(package)

dist: $(distdir).tar.gz

$(distdir).tar.gz: $(distdir)
	tar chzf - $(distdir) > $@
	rm -rf $(distdir)

$(distdir): FORCE
	mkdir -p $(distdir)
	cp configure.ac $(distdir)
	cp configure $(distdir)
	cp autogen.sh $(distdir)
	cp Makefile.in LICENSE README README.md $(distdir)
	cp *.[ch] $(distdir)

Makefile: Makefile.in config.status
	./config.status $@

config.status: configure
	./config.status --recheck

clean:
	rm -f *.o
	rm -f *~
	rm -f core
	rm -f $(package)

FORCE:
	-rm $(distdir).tar.gz >/dev/null 2>&1
	-rm -rf $(distdir) >/dev/null 2>&1

.PHONY: FORCE all clean install uninstall
